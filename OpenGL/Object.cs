﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace OpenGL
{
	public class Object
	{
		public Object(int x, int y, int h, int w, Color rgb, int z)
		{
			X = x;
			Y = y;
			H = h;
			W = w;
			RGB = rgb;
			Zindex = z;
			paint = new Paint();
			paint.Color = RGB;
		}

		public int X { get; set; }
		public int Y { get; set; }
		public int H { get; set; }
		public int W { get; set; }
		public int Zindex { get; set; }
		public Color RGB { get; set; }
		public Paint paint { get; set; }

		public void OnDraw(Canvas canvas)
		{
			canvas.DrawRect(X, Y + H, Y, X + W, paint);
		}

	}
}