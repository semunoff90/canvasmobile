﻿using System;
using System.Timers;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace OpenGL
{

    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {

	    private DrawCanvas canvas;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            canvas = new DrawCanvas(this);

            CoordinatorLayout layout = (CoordinatorLayout) FindViewById(Resource.Id.mainlayout);

            layout.AddView(canvas);

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1;
            timer.Elapsed += OnTimedEvent;
            timer.Enabled = true;

        }

		private void OnTimedEvent(object sender, ElapsedEventArgs e)
		{
			canvas.Invalidate();
		}

		public override bool OnTouchEvent(MotionEvent e)
		{
			return true;
		}
    }
}
