﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace OpenGL
{
	public class DrawCanvas : View
	{
		protected DrawCanvas(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}

		public DrawCanvas(Context context) : base(context)
		{
		}

		public DrawCanvas(Context context, IAttributeSet attrs) : base(context, attrs)
		{
		}

		public DrawCanvas(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
		{
		}

		public DrawCanvas(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
		{
		}

		public List<Object> Objs = new List<Object>(new []{ new Object(0, 0, 100, 300, Color.Blue, 1), 
			new Object(300, 200, 300, 200, Color.Red, 2) });

		protected override void OnDraw(Canvas canvas)
		{
			base.OnDraw(canvas);
			Objs.Sort((x, y) => x.Zindex.CompareTo(y.Zindex));
			foreach (var obj in Objs)
			{
				obj.OnDraw(canvas);
			}
		}
	}
}